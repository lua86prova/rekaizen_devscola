#!/bin/sh

ssh -t -oStrictHostKeyChecking=no debian@51.89.165.239 "
  cd /home/debian/rekaizen_devscola &&
  git pull && docker-compose down &&
  docker-compose -f docker-compose.production.yml up --build -d
"
