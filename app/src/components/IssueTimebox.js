import React from 'react'
import IssueTimeboxButton from './IssueTimeboxButton'
import LogoRe from './LogoRe'
import Button from './Button'

class IssueTimebox extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showMoreOptions: false,
      showMoreOptionsButton: true,
    }
  }

  build(options){
    return options.map(option => (
        <IssueTimeboxButton
          key={`timebox-option-${option}`}
          id={`timebox-option-${option}`}
          onClick={this.selectTimebox.bind(this)}
          time={option}
        />
      )
    )
  }

  selectTimebox(time) {
    this.props.onSubmit(time)
  }

  handleShowMoreOptions() {
    this.setState({
      showMoreOptions: true,
      showMoreOptionsButton: false,
    })
  }

  renderMoreOptions() {
    const moreOptions = this.build(this.props.moreOptions)
    const hidden = this.state.showMoreOptions? '' : 'hidden'
    return (
      <div id="more-options" className={hidden}>
        {moreOptions}
      </div>
    )
  }
  showMoreOptionsButton() {
    if (this.state.showMoreOptionsButton) {
      return (
        <button
          id="options"
          className="button-more-timebox-options"
          onClick={this.handleShowMoreOptions.bind(this)}
        >
          <i className="fas fa-plus fa-lg"></i>
        </button>
      )
    }
    return null
  }

  render() {
    const options = this.build(this.props.options)
    return (
      <div className="slide">
        <LogoRe />
        <div className="modal-container">
          <h2 className="title">{this.props.translations.timebox}</h2>
          <p className="text landing-description">
            {this.props.translations.selectTimebox}
          </p>
          <div className="row-timebox-options">
            {options}
            {this.showMoreOptionsButton()}
          </div>
          {this.renderMoreOptions()}
          <div>
            <Button
              link
              onClick={this.props.back}
            >
              {this.props.translations.back}
            </Button>
          </div>
        </div>
      </div>
    )
  }
}

IssueTimebox.defaultProps = {
  options: [],
  moreOptions: [60]
}

export default IssueTimebox
