import React from 'react'
import Button from './Button'
import List from './List'
import ModalColumn from './ModalColumn'

class ColumnConclusion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
    }
  }
  create(description) {
    this.props.create(description)
    this.closeModal()
  }
  openModal() {
    this.setState({ showModal: true })
  }
  closeModal() {
    this.setState({ showModal: false })
  }
  renderModal() {
    return this.state.showModal?
      <ModalColumn
        title={this.props.translations.modalTitle}
        textHelp={this.props.translations.submitConclusionHelp}
        textSubmit={this.props.translations.submitConclusion}
        onClose={this.closeModal.bind(this)}
        onSubmit={this.create.bind(this)}
      />
    : null
  }
  render() {
    return (
      <div className="column-inner column-minimum-height">
        {this.renderModal()}
        <div className="list-header">
          <div className="list-title">
            {this.props.translations.listTitle}
          </div>
          <Button
            primary
            id={"new-" + this.props.id}
            onClick={this.openModal.bind(this)}
          >
            {this.props.translations.add}
          </Button>
        </div>
        <List
          id={this.props.id}
          icon={<i className="icon fas fa-chevron-right" />}
          collection={this.props.collection}
        />
      </div>
    )
  }
}

ColumnConclusion.defaultProps = {
  collection: [],
  translations: {},
}

export default ColumnConclusion
