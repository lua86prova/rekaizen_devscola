import Button from './Button'
import React from 'react'
import List from './List'
import TextareaAutosize from './TextareaAutosize'


class ColumnAnalysis extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      visible: false,
    }
  }

  create() {
    if(this.state.value) {
      this.props.create(this.state.value)
      this.setState({ value: '' })
    }
  }

  show() {
    this.setState({ visible: true })
  }

  hide() {
    this.setState({
      value: '',
      visible: false,
    })
  }

  handleChange(event) {
    this.setState({ value: event.target.value })
  }

  handleKeyDown(event) {
    if(event.key === "Escape") {
      event.preventDefault()
      this.hide()
    }
    if(event.key === "Enter") {
      event.preventDefault()
      this.create()
    }
  }

  render() {
    return (
      <div className="analyses-column column-inner">
        <div className="list-header">
          <div className="list-title">
            {this.props.translations.listTitle}
          </div>
        </div>
        <List
          bordered
          icon={this.props.icon}
          id={this.props.id}
          collection={this.props.collection}
          remove={this.props.remove}
        />
        {this.state.visible ?
          <TextareaAutosize
            autoFocus
            className="background-highlight"
            value={this.state.value}
            onKeyDown={this.handleKeyDown.bind(this)}
            onBlur={this.hide.bind(this)}
            onChange={this.handleChange.bind(this)}
          />
          :
          <div className="has-text-right">
            <Button
              text
              id={`${this.props.id}-add`}
              onClick={this.show.bind(this)}
            >
              {this.props.translations.add}
            </Button>
          </div>
        }
      </div>
    )
  }
}

ColumnAnalysis .defaultProps = {
  translations: {},
  collection: [],
}

export default ColumnAnalysis
