import React from 'react'
import LogoRe from './LogoRe'

class IssueTimeboxButton extends React.Component {
  handleClick() {
    this.props.onClick(this.props.time)
  }
  render() {
    return (
      <button
        className="button-timebox-option"
        id={this.props.id}
        onClick={this.handleClick.bind(this)}
      >
        <LogoRe
          height="56"
          width="54"
          time={this.props.time}
        />
      </button>
    )
  }
}

export default IssueTimeboxButton
