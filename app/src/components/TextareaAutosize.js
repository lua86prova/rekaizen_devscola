import React from 'react'

const PADDING = 16
const TEXT_AREA_LINE_HEIGHT = 20
const DEFAULT_ROWS = 3
const MAX_ROWS = 10

class TextareaAutosize extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      minRows: this.props.rows,
      rows: this.props.rows,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if(props.value.length === 0) {
      return {
        rows: state.minRows,
      }
    }
    return state
  }

  handleChange(event) {
    const previousRows = event.target.rows
    event.target.rows = this.state.minRows
    const actualHeight = event.target.scrollHeight - PADDING
    const currentRows = Math.floor(actualHeight / TEXT_AREA_LINE_HEIGHT)

    if (currentRows === previousRows) {
      event.target.rows = currentRows
    }

    if (currentRows >= MAX_ROWS) {
      event.target.rows = MAX_ROWS
      event.target.scrollTop = event.target.scrollHeight
    }

    const rows = currentRows < MAX_ROWS ? currentRows : MAX_ROWS
    this.setState({ rows })
    this.props.onChange(event)
  }
  
  render() {
    const { onChange, rows, ...props } = this.props
    return (
      <textarea
        className="textarea"
        rows={this.state.rows}
        onChange={this.handleChange.bind(this)}
        {...props}
      />
    )
  }
}

TextareaAutosize.defaultProps = {
  rows: DEFAULT_ROWS,
  onChange: () => {}
}

export default TextareaAutosize