import React from 'react'

class Notification extends React.Component {
  render() {
    if (!this.props.show) { return null }
    return (
      <div className="notification no-print">
        {this.props.text}
      </div>
    )
  }
}

Notification.defaultProps = {
  show: false,
  text: ''
}

export default Notification
