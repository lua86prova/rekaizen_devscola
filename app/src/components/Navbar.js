import React from 'react'
import Timebox from './Timebox'
import Notification from './Notification'
import Button from './Button'

class Navbar extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Timebox timebox={this.props.timebox} />
        <div className="navbar-actions">
          <span className="navbar-issue-description">{this.props.description}</span>
          <Button
            ceremonial
            id="create-report-button"
            onClick={this.props.resumeIssue}
          >
            {this.props.translations.finish}
          </Button>
        </div>
        <Notification
          text={this.props.text}
          show={this.props.showNotificaiton}
        />
      </React.Fragment>
    )
  }
}

export default Navbar
