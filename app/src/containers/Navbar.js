import Renderers from '../renderers'
import Container from './Container'

class Navbar extends Container {
  constructor(bus){
    super(bus)
    const callbacks = {
      resumeIssue: this.resumeIssue.bind(this)
    }
    this.renderer = Renderers.navbar(callbacks)
  }

  subscriptions() {
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('created.goodPractice', this.showAlertGoodPractice.bind(this))
    this.bus.subscribe('created.lessonLearned', this.showAlertLessonLearned.bind(this))
    this.bus.subscribe('created.issue', this.startTimebox.bind(this))
    this.bus.subscribe('restore.issue', this.startTimebox.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  startTimebox(issue) {
    this.retrieveIssue(issue)
    this.renderer.setIssueDescription(issue.description)
    this.renderer.setTimebox(issue.timebox)
  }

  resumeIssue() {
    this.bus.publish("generate.report", {issue: this.issue.id})
  }

  showAlertGoodPractice() {
    this.renderer.alertGoodPracticeAdded()
  }

  showAlertLessonLearned() {
    this.renderer.alertLessonLearnedAdded()
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.setIssueDescription('')
    this.renderer.setTimebox(0)
  }

  render() {
    this.renderer.draw()
  }
}

export default Navbar
