
class Container {
  constructor(bus){
    this.bus = bus
    this.issue = {}
    this.subscriptions()
  }

  subscriptions() {
    throw('Implement #subscriptions method')
  }

  retrieveIssue(issue) {
    this.issue = issue
  }

  updateTranslations(payload) {
    this.renderer.translate(payload)
  }

  render() {
    throw('Implement #render method')
  }
}

export default Container
