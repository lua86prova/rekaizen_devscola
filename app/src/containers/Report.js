import Container from './Container'
import Renderers from '../renderers'

class Report extends Container {
  constructor(bus) {
    super(bus)
    const callbacks = {
      newIssue: this.newIssue.bind(this),
      finishRetro: this.finishRetro.bind(this),
    }
    this.renderer = Renderers.report(callbacks)
  }

  subscriptions() {
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('generated.report', this.showReport.bind(this))
  }

  showReport(report) {
    this.renderer.showReport(report, this.issue.description)
  }

  newIssue() {
    this.bus.publish('new.issue')
  }

  finishRetro() {
    this.newIssue()
  }

  render() {
    this.renderer.draw()
  }
}

export default Report
