import Renderers from '../renderers'
import Container from './Container'
import Orderer from '../libraries/Orderer'

class Negatives extends Container {
  constructor(bus){
    super(bus)
    const callbacks = {
      create: this.createNegative.bind(this),
      remove: this.removeNegative.bind(this)
    }
    this.renderer = Renderers.negatives(callbacks)
  }

  subscriptions() {
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.askNegatives.bind(this))
    this.bus.subscribe('retrieved.negatives', this.updateNegatives.bind(this) )
    this.bus.subscribe('removed.negative', this.askCollection.bind(this) )
    this.bus.subscribe('created.negative', this.askCollection.bind(this))
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  askNegatives(issue) {
    this.retrieveIssue(issue)
    this.bus.publish("retrieve.negatives", {issue: this.issue.id})
  }

  askCollection() {
    this.bus.publish("retrieve.negatives", {issue: this.issue.id})
  }

  updateNegatives(payload) {
    let orderedPayload = Orderer.byBirthdayOldestToNewest(payload)
    this.renderer.update(orderedPayload)
  }

  createNegative(value) {
    let payload = { description: value, issue: this.issue.id }
    this.renderer.updateOne(payload)
    this.bus.publish("create.negative", payload)
  }

  removeNegative(negative) {
    const payload = negative
    payload['issue'] = this.issue.id

    this.bus.publish('remove.negative', payload)
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.update([])
  }

  render() {
    this.renderer.draw()
  }
}

export default Negatives
