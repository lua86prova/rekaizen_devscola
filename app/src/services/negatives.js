import Service from './Service'

export default class Negatives extends Service {
  subscriptions() {
    this.bus.subscribe('create.negative', this.create.bind(this))
    this.bus.subscribe('retrieve.negatives', this.retrieveAllFor.bind(this))
    this.bus.subscribe('remove.negative', this.removeNegative.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.negative')
    let body = payload
    let url = 'createNegative'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallbackRetrieve.bind(this)
    let body = payload
    let url = 'retrieveAnalyses'
    this.client.hit(url, body, callback)
  }

  removeNegative(payload) {
    let callback = this.buildCallback('removed.negative')
    let body = payload
    let url = 'removeAnalysis'
    this.client.hit(url, body, callback)
  }

  buildCallbackRetrieve(response) {
    this.bus.publish('retrieved.negatives', response.negatives)
}
}
