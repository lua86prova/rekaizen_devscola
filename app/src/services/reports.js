import Service from './Service'

class Reports extends Service {
  subscriptions() {
    this.bus.subscribe('generate.report', this.generateReport.bind(this))
  }

  error() {
    this.bus.publish('new.issue')
  }

  generateReport(payload) {
    const callback = this.buildCallback('generated.report')
    const body = payload
    const url = 'generateReport'
    this.client.hit(url, body, callback, this.error.bind(this))
  }
}

export default Reports
