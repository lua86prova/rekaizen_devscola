import Translations from './translations'
import ImprovementActions from './ImprovementActions'
import GoodPractices from './goodPractices'
import LessonsLearned from './lessonsLearned'
import Positives from './positives'
import Negatives from './negatives'
import Timeboxes from './timeboxes'
import Reports from './reports'
import Issues from './issues'

class Services {
  static translations(bus) {
    return new Translations(bus)
  }

  static improvementActions(bus) {
    return new ImprovementActions(bus)
  }

  static goodPractices(bus) {
    return new GoodPractices(bus)
  }

  static lessonsLearned(bus) {
    return new LessonsLearned(bus)
  }

  static positives(bus) {
    return new Positives(bus)
  }

  static negatives(bus) {
    return new Negatives(bus)
  }

  static timeboxes(bus) {
    return new Timeboxes(bus)
  }

  static reports(bus) {
    return new Reports(bus)
  }

  static issues(bus) {
    return new Issues(bus)
  }
}

export default Services
