import Service from './Service'

export default class Translations extends Service {
  constructor(bus) {
    super(bus)
    this.retrieveTranslations()
  }
  
  retrieveTranslations() {
    let callback = this.buildCallback('got.translations')
    let body = {}
    let url = 'translate'
    this.client.hit(url, body, callback)
  }
}
