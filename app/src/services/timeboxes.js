import Service from './Service'

export default class Timeboxes extends Service {
  constructor(bus) {
    super(bus)
    this.retrieveTimeboxes()
  }

  retrieveTimeboxes() {
    let callback = this.buildCallback('got.timeboxes')
    let body = {}
    let url = 'retrieveTimeboxes'
    this.client.hit(url, body, callback)
  }
}
