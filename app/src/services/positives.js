import Service from './Service'

export default class Positives extends Service {
  subscriptions() {
    this.bus.subscribe('create.positive', this.create.bind(this))
    this.bus.subscribe('remove.positive', this.removePositive.bind(this))
    this.bus.subscribe('retrieve.positives', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.positive')
    let body = payload
    let url = 'createPositive'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    const callback = this.buildCallbackRetrieve.bind(this)
    let body = payload
    let url = 'retrieveAnalyses'
    this.client.hit(url, body, callback)
  }

  removePositive(payload) {
    let callback = this.buildCallback('removed.positive')
    let body = payload
    let url = 'removeAnalysis'
    this.client.hit(url, body, callback)
  }

  buildCallbackRetrieve(response) {
    this.bus.publish('retrieved.positives', response.positives)
  }
}
