import Containers from './containers'
import Services from './services'
import Bus from './infrastructure/bus'

const bus = new Bus()

Services.issues(bus)
Services.translations(bus)
Services.improvementActions(bus)
Services.goodPractices(bus)
Services.lessonsLearned(bus)
Services.positives(bus)
Services.negatives(bus)
Services.timeboxes(bus)
Services.reports(bus)

Containers.improvementActions(bus).render()
Containers.lessonsLearned(bus).render()
Containers.goodPractices(bus).render()
Containers.positives(bus).render()
Containers.negatives(bus).render()
Containers.navbar(bus).render()
Containers.report(bus).render()
Containers.landing(bus).render()
Containers.issue(bus).render()

Containers.restoreIssue(bus)
