import Bus from '../../src/infrastructure/bus'
import Callback from '../components/CallBack'

describe('Bus', () => {
  it('can publish a topic without subscriptions', () => {
    const bus = new Bus()

    expect(() => {
      bus.publish('anoter.topic')
    }).not.toThrow(TypeError)

  })

  it('executes all the actions subscribed to a topic when it is published with a message', () => {
    const bus = new Bus()
    const message = 'Any message'
    const callback = new Callback()
    bus.subscribe('a.topic', callback.toBeCalled.bind(callback))

    bus.publish('a.topic', message)

    expect(callback.hasBeenCalledWith()).toEqual(message)
  })
})
