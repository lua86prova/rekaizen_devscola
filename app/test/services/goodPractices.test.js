import GoodPractices from '../../src/services/goodPractices'
import Subscriber from './subscriber'
import StubAPI from './stubApi'
import NewBus from '../../src/infrastructure/bus'

describe('Good Practices Service', () => {
  it('creates a good practice', () => {
    const bus = new NewBus()
    const subscriber = new Subscriber('created.goodPractice', bus)
    const service = new GoodPracticesTest(bus)

    bus.publish('create.goodPractice', { description: 'A Good Practice' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the good practices', () => {
    const bus = new NewBus()
    const subscriber = new Subscriber('retrieved.goodPractices', bus)
    const service = new GoodPracticesTest(bus)

    bus.publish('retrieve.goodPractices')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

class GoodPracticesTest extends GoodPractices {
  constructor (bus) {
    super(bus)
    this.client = new StubAPI()
  }

}
