const Conclusions = require ('../../services/conclusions')
const {expect} = require('chai')

describe('Conclusions Service', () => {
  beforeEach(()=>{
    Conclusions.collection.drop()
  })

  it('creates a good practices', () => {
    const description = 'a good practice'

    let created = Conclusions.service.createGoodPractice(description)

    let retrieved = Conclusions.service.retrieveAllGoodPractices([ created.id ])
    expect(retrieved.length).to.eq(1)
  })

  it('creates a lesson learned', () => {
    const description = 'a good practice'

    let lessonLearned = Conclusions.service.createLessonLearned(description)

    let retrieved = Conclusions.service.retrieveAllLessonsLearned([lessonLearned.id])
    expect(retrieved.length).to.eq(1)
  })
})
