const Issues = require ('../../services/issues')
const IssueBuilder = require ('../helpers/IssueBuilder')
const {expect} = require('chai')

describe('Issues Service', () => {
  beforeEach(()=>{
    Issues.collection.drop()
  })

  it('adds conclusions to a Issue', () => {
    const issue = IssueBuilder.withDefaultValues().build()
    Issues.collection.store(issue)
    const conclusionBirthday = '10/02/2019'

    const issueWithConclusion = Issues.service.addConclusion(issue.id, conclusionBirthday)

    expect(issueWithConclusion.conclusions[0]).to.eq(conclusionBirthday)
  })

  it('stores finalization moment', () => {
    const timestamp = 'number'
    const issue = IssueBuilder.withDefaultValues().build()
    Issues.collection.store(issue)

    const closedIssue = Issues.service.finalize(issue.id)

    expect(closedIssue.finalizedAt).to.be.a(timestamp)
  })
})
