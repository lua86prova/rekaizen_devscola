const GenerateReport = require('../../actions/GenerateReport')
const Issues = require ('../../services/issues')
const Conclusions = require ('../../services/conclusions')
const IssueBuilder = require ('../helpers/IssueBuilder')
const { expect } = require('chai')

describe('GenerateReport', () => {
  beforeEach(()=>{
    Issues.collection.drop()
    Conclusions.collection.drop()
  })

  it('contains the title', () => {
    const issue = IssueBuilder.withDefaultValues().build()

    const report = GenerateReport.do(issue.id)

    expect(report.title).to.eq('Resumen')
  })

  it('contains an introduction', () => {
    const issue = IssueBuilder.withDefaultValues().build()

    const report = GenerateReport.do(issue.id)

    expect(report.introduction).to.include('Hemos analizado el tema')
  })

  it("the introduction contains the issue's name", () => {
    const issue = IssueBuilder.withDefaultValues().build()
    const description = issue.description

    const report = GenerateReport.do(issue.id)

    expect(report.introduction).to.include(description)
  })

  it("knows the number of positive analyses", () => {
    const issue = IssueBuilder.withDefaultValues().withPositives().build()
    const twoPositiveImpresions = `${issue.analyses.length} impresiones`

    const report = GenerateReport.do(issue.id)

    expect(report.introduction).to.include(twoPositiveImpresions)
  })

  it("knows the number of negative analyses", () => {
    const issue = IssueBuilder.withDefaultValues().withNegatives().build()
    const twoNegativeImpresions = `${issue.analyses.length} negativas`

    const report = GenerateReport.do(issue.id)

    expect(report.introduction).to.include(twoNegativeImpresions)
  })

  it("knows if there are one or more conclusions in the report introduction", () => {
    const issue = IssueBuilder.withDefaultValues().withLessonsLearned().build()
    const twoConclusions = "llegar a conclusiones"

    const report = GenerateReport.do(issue.id)

    expect(report.introduction).to.include(twoConclusions)
  })

  it("knows how to write the sentence depending on the number of good practices", () => {
    const issue = IssueBuilder.withDefaultValues().withGoodPractices().build()
    const twoGoodPractices = "estas prácticas"

    const report = GenerateReport.do(issue.id)

    expect(report.goodPracticesIntro).to.include(twoGoodPractices)
  })

  it("knows how to retrieve the description of a good practice", () => {
    const issue = IssueBuilder.withDefaultValues().withGoodPractices().build()
    const goodPracticesList = Conclusions.service.retrieveAllGoodPractices(issue.conclusions)
    const goodPractice = goodPracticesList[0].description

    const report = GenerateReport.do(issue.id)

    expect(report.goodPractices).to.include(goodPractice)
  })

  it("knows to write the sentence depends of the number of the lessons learned", () => {
    const issue = IssueBuilder.withDefaultValues().withLessonsLearned().build()
    const moreThanOneLessonLearned = "aprendizajes"

    const report = GenerateReport.do(issue.id)

    expect(report.lessonsLearnedIntro).to.include(moreThanOneLessonLearned)
  })

  it("knows how to retrieve the list of lessons learned", () => {
    const issue = IssueBuilder.withDefaultValues().withLessonsLearned().build()
    const lessonsLearnedList = Conclusions.service.retrieveAllLessonsLearned(issue.conclusions)
    const aLessonLearned = lessonsLearnedList[0].description


    const report = GenerateReport.do(issue.id)

    expect(report.lessonsLearned).to.include(aLessonLearned)
  })
})
