const ImprovementActions = require('../../services/improvementActions')
const { expect } = require('chai')

describe('Improvement Actions Service', () => {
  beforeEach(() => {
    ImprovementActions.collection.drop()
  })

  after(() => {
    ImprovementActions.collection.drop()
  })

  it('creates an Improvement Action', () => {
    const description = 'Improvement Action'

    const stored = ImprovementActions.service.create(description)

    const collected = ImprovementActions.service.retrieve([stored.id])
    expect(collected.length).to.eq(1)
  })
})
