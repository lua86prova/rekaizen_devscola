const chaiHttp = require('chai-http')
const api = require('../api')
const {expect} = require('chai')
const chai = require('chai')

const Issues = require('../services/issues')
const Actions = require('../actions')
const Analysis = require('../domain/Analysis')
const IssueBuilder = require('./helpers/IssueBuilder')

chai.use(chaiHttp)

describe('Improvement actions endpoints', () => {
  beforeEach(() => {
    Issues.collection.drop()
  })
  after(() => {
    Issues.collection.drop()
  })

  it('remove improvement action', (done) => {
    const issue = IssueBuilder.withDefaultValues().withImprovementActions().build()
    const request = {
      id: issue.improvementActions[0],
      issue: issue.id
    }

    chai.request(api)
      .post('/removeImprovementAction')
      .send(request)
      .end((_, response) => {
        const body = JSON.parse(response.text)
        expect(body.improvementActions.length).to.eq(1)
        done()
      })
    })
  })
