const chaiHttp = require('chai-http')
const api = require('../api')
const {expect} = require('chai')
const chai = require('chai')

const Issues = require('../services/issues')
const Actions = require('../actions')
const Analysis = require('../domain/Analysis')

chai.use(chaiHttp)

describe('Analyses endpoints', () => {
  beforeEach(() => {
    Issues.collection.drop()
  })
  after(() => {
    Issues.collection.drop()
  })
  it('retrieves all analyses', (done) => {
    const issue = Actions.createIssue.do({ issue: 'IssueDescription', timebox: 10 })
    const positive = Issues.service.createPositive(issue.id, 'a positive')
    const negative = Issues.service.createNegative(issue.id, 'a negative')

    chai.request(api)
      .post('/retrieveAnalyses')
      .send({ issue: issue.id })
      .end((_, response) => {
        const body = JSON.parse(response.text)
        expect(body).to.deep.eq({
          positives: [positive],
          negatives: [negative]
        })
        done()
      })
  })
  it('remove analysis', (done) => {
    const issue = Actions.createIssue.do({ issue: 'IssueDescription', timebox: 10 })
    const positive = Issues.service.createPositive(issue.id, 'a positive')
    const request = {...positive, issue: issue.id }

    chai.request(api)
      .post('/removeAnalysis')
      .send(request)
      .end((_, response) => {
        const body = JSON.parse(response.text)
        expect(body.analyses).to.be.empty
        done()
      })
  })
})
