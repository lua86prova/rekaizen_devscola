const expect = require('chai').expect
const Interpolate = require('../../libraries/Interpolate')

describe('Interpolate', () => {
  it('strings in keys', () => {
    const word = 'word'
    const anotherWord = 'anotherWord'

    const result = Interpolate.withKeys(`%word %anotherWord`, { word, anotherWord })
    
    expect(result).to.eq(`${word} ${anotherWord}`)
  })
})

