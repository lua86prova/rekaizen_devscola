const Analysis = require ('../../domain/Analysis')
const Dates = require('../../libraries/Dates')
const expect = require('chai').expect

describe('Analysis', () => {
  it('is timestamped at inicialization', () =>{
    const now = Dates.generateTimestamp()

    const anAnalysis = new AnalysisTest()

    const then = Dates.generateTimestamp()
    expect(anAnalysis.isBirthdayBetween(now, then)).to.be.true
  })

  it('needs a description', () =>{
    const description = "A description"

    const anAnalysis = new AnalysisTest(description)

    expect(anAnalysis.hasDescription(description)).to.be.true
  })

  it('can be serialized', () =>{
    const description = "A description"
    const anAnalysis = new Analysis(description)

    expect(anAnalysis.serialize()).to.have.keys(['description', 'type', 'birthday', 'id'])
  })

  it('knows if is equal to another analysis', () =>{
    const anAnalysis = new Analysis()

    expect(anAnalysis.isEqualTo(anAnalysis)).to.be.true
  })

  it('can be created as null', () => {
    const nullAnalysis = { type: null, description: null, birthday: null }

    const analysis = Analysis.asNull()

    expect(analysis.isEqualTo(nullAnalysis)).to.be.true
  })

  it('can be created as positive', () => {

    const analysis = Analysis.asPositive('description')

    expect(analysis.isPositive()).to.be.true
  })

  it('can be created as negative', () => {

    const analysis = Analysis.asNegative('description')

    expect(analysis.isNegative()).to.be.true
  })
  it('has an uuid as id', () =>{
    const uuid = 'uuid'

    const analysis = new AnalysisTest()

    expect(analysis.serialize().id).to.eq(uuid)
  })
})

class AnalysisTest extends Analysis {
  isBirthdayBetween(now, then) {
    return this.birthday >= now && this.birthday <= then
  }

  hasDescription(text) {
    return this.description == text
  }

  generateId() {
    return 'uuid'
  }
}
