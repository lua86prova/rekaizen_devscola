const Conclusion = require ('../../domain/Conclusion')
const Dates = require('../../libraries/Dates')
const {expect} = require('chai')


describe('Conclusion', () => {
  it('is timestamped at inicialization', () =>{
    const now = Dates.generateTimestamp()

    const conclusion = new ConclusionTest()

    const then = Dates.generateTimestamp()
    expect(conclusion.isBirthdayBetween(now, then)).to.be.true
  })

  it('has a description', () =>{
    const description = "A description"

    const conclusion = new ConclusionTest(description)

    expect(conclusion.hasDescription(description)).to.be.true
  })

  it('has an uuid as id', () => {
    const uuid = 'uuid'

    const conclusion = new ConclusionTest()

    expect(conclusion.serialize().id).to.eq(uuid)
  })

  it('can be serialized', () =>{
    const description = "A description"
    const conclusion = new Conclusion(description)

    const serialization = conclusion.serialize()

    expect(serialization).to.have.keys(['description', 'birthday', 'id'])
  })

  it('can be created as GoodPractice', () => {
    const description = 'A description'
    const issue = 'aIssue'

    const conclusion = Conclusion.asGoodPractice(description, issue)

    expect(conclusion.isAGoodPractice()).to.be.true
  })

  it('knows if is not a GoodPractice', () => {
    const description = 'A description'
    const issue = 'aIssue'

    const conclusion = new Conclusion(description, issue)

    expect(conclusion.isAGoodPractice()).to.be.false
  })

  it('can be created as LessonLearned', () => {
    const description = 'A description'
    const issue = 'aIssue'

    const conclusion = Conclusion.asLessonLearned(description, issue)

    expect(conclusion.isALessonLearned()).to.be.true
  })
})

class ConclusionTest extends Conclusion {
  generateId() {
    return 'uuid'
  }
  isBirthdayBetween(now, then) {
    return this.birthday >= now && this.birthday <= then
  }
  hasDescription(text) {
    return this.description == text
  }
}
