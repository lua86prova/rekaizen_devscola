const {expect} = require('chai')
const Collection = require('../../services/issues').collection
const IssueBuilder = require('../helpers/IssueBuilder')

describe('Issue', () => {
  it ('remove analysis', () => {
    const issue = IssueBuilder.withDefaultValues().withPositives().build()
    const analysis = issue.analyses[0]
    const domainIssue = Collection.retrieve(issue.id)

    domainIssue.removeAnalysis(analysis.id)

    expect(domainIssue.analyses.length).to.eq(1)
  })
})
