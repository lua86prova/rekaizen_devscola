const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/retrieveTimeboxes', function (req, res) {
  const message = Actions.retrieveTimeboxes.do()

  res.send(JSON.stringify(message))
})

module.exports = router
