const Actions = require('../actions')

const express = require('express')
const router = express.Router()

router.post('/createPositive', function (req, res) {
  const positive = req.body
  const message = Actions.createPositive.do(positive)

  res.send(JSON.stringify(message))
})

router.post('/retrieveAnalyses', function(req, res) {
  const issue = req.body
  const analyses = Actions.retrieveAnalyses.do(issue.issue)
  res.send(JSON.stringify(analyses))
})

router.post('/removeAnalysis', function(req, res) {
  const analysis = req.body
  const message = Actions.removeAnalysis.do(analysis)
  res.send(JSON.stringify(message))
})

router.post('/createNegative', function (req, res) {
  const negative = req.body
  const message = Actions.createNegative.do(negative)

  res.send(JSON.stringify(message))
})

module.exports = router
