const GenerateReport = require('../actions/GenerateReport')

const express = require('express')
const router = express.Router()

router.post('/generateReport', function (req, res) {
  const issue = req.body
  const report = GenerateReport.do(issue.issue)

  res.send(JSON.stringify(report))
})

module.exports = router
