const Collection = require('./collection')
const Service = require('./service')

const Conclusions = {
  collection: Collection,
  service: Service
}

module.exports = Conclusions
