const ImprovementAction = require('../../domain/ImprovementAction')
const Collection = require('./collection')

class Service {
  static create(description) {
    const improvementAction = new ImprovementAction(description)

    const stored = Collection.insert(improvementAction)

    return stored.serialize()
  }

  static retrieve(references) {
    const retrieved = Collection.retrieve(references)

    return retrieved.map((improvementAction) => {
      return improvementAction.serialize()
    })
  }

  static remove(id) {
    return Collection.remove(id)
  }
}

module.exports = Service
