const Collection = require('./collection')
const Service = require('./service')

const ImprovementActions = {
  service: Service,
  collection: Collection
}

module.exports = ImprovementActions
