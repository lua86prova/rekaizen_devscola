class Collection {
  static retrieveAll () {
    return {
      rekaizen: "ReKaizen",
      improvementActions: "Acciones de mejora",
      goodPractices: "Buenas prácticas",
      goodPractice: "Buena práctica",
      goodPracticeAdded: "¡Habéis declarado una buena práctica!",
      goodPracticeCeremonial: "Estáis declarando una buena práctica que queréis mantener a partir de ahora",
      lessonsLearned: "Aprendizajes",
      lessonLearned: "Aprendizaje",
      lessonLearnedAdded: "¡Habéis declarado un nuevo aprendizaje!",
      lessonLearnedCeremonial: "Estáis declarando un aprendizaje que habéis interiorizado a través de vuestra experiencia",
      submitConclusion: "Hay consenso",
      add: "Declarar",
      back: "Atrás",
      propose: "Proponer",
      positives: "Puntos positivos",
      addPositive: "+ Añadir",
      negatives: "Puntos negativos",
      addNegative: "+ Añadir",
      timebox: "Timebox",
      selectTimebox: "Elegid el tiempo máximo para analizar el tema.",
      confirm: "Confirmar",
      moreOptions: "Más opciones",
      lessOptions: "Menos opciones",
      finish: "Lo tenemos",
      newIssue: "Nuevo tema",
      finishRetro: "Hemos terminado la ceremonia",
      reportTitle: "Resumen",
      landingSlogan: "Make a good retrospective",
      landingCopy:" Hacer una buena retrospectiva pasa por algo más que utilizar un simple tablero. Rekaizen propone reunir la experiencia de los mejores equipos para aquellos que quieran abrazar la mejora continua.",
      landingExplanation: "Estáis a punto de empezar la Retrospectiva",
      start: "Estamos preparados",
      issueTitle: "Tema",
      issueSubtitle: 'Elegid el tema que vais a analizar.',
      confirmIssue: "Podemos empezar",
      confirmIssueSubtitle: "Estáis a punto de iniciar el análisis"
    }
  }

  static retrieveReportTranslations () {
    return {
      reportTitle: "Resumen",
      introduction: "Hemos analizado el tema \"%description\"",
      positivesImpresionsSentenceForZero: " y después de no coleccionar impresiones positivas",
      positivesImpresionsSentenceForOne: " y coleccionado una impresión positiva",
      positivesImpresionsSentenceForMore: " y coleccionado %number impresiones positivas",
      negativeImpressionsSentenceForZero: " y ninguna negativa.",
      negativeImpressionsSentenceForOne: " y una negativa.",
      negativeImpressionsSentenceForMore: " y %number negativas.",
      notAnalysesSentence: " y no hemos coleccionado ninguna impresión.",
      conclusionsSentenceForZero: "Al final, no hemos logrado llegar a ninguna conclusión.",
      conclusionsSentenceForOne: "A través de este análisis hemos sido capaces de llegar a una conclusión de manera consensuada.",
      conclusionsSentenceForMore: "A través de este análisis hemos sido capaces de llegar a conclusiones de manera consensuada.",
      goodPracticesSentenceForOne: "Hemos descubierto que esta práctica está teniendo un impacto positivo y queremos mantenerla a partir de ahora:",
      goodPracticesSentenceForMore: "Hemos descubierto que estas prácticas están teniendo un impacto positivo y queremos mantenerlas a partir de ahora:",
      lessonsLearnedSentenceForOne: "Hemos declarado este aprendizaje que ya forma parte de nuestro nuestro ADN:",
      lessonsLearnedSentenceForMore: "Hemos declarado estos aprendizajes que ya forman parte de nuestro nuestro ADN:",
      improvementActionsSentenceForOne: "Queremos probar esta acción de mejora y analizar su impacto en la próxima retrospectiva:",
      improvementActionsSentenceForMore: "Queremos probar estas acciones de mejora y analizar su impacto en la próxima retrospectiva:",
      endingDate: "Esta retrospectiva se celebró el día %stringDate.",
      endingTime: "Teníamos pensado dedicarle %timebox a este tema y finalmente consumimos %spentTime.",
      timeWithMinute: "%time minuto",
      timeWithMinutes: "%time minutos"
    }
  }
}

module.exports = Collection
