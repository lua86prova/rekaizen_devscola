const Issue = require('../../domain/Issue')

let collection = []
class Collection {
  static store(issue) {
    collection.push(issue)

    return issue
  }

  static retrieve(issue) {

    let found = collection.find((element) => element.isEqualTo(issue))

    return found
  }

  static drop() {
    collection = []
  }
}

module.exports = Collection
