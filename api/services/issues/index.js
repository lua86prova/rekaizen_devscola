const Collection = require('./collection')
const Service = require('./service')

const Issues = {
  collection: Collection,
  service: Service
}

module.exports = Issues
