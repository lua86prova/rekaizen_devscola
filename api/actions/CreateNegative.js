const Issues = require('../services/issues')


class CreateNegative {
  static do(negative) {
    const newNegative = Issues.service.createNegative(negative.issue, negative.description)

    return newNegative
  }
}

module.exports = CreateNegative
