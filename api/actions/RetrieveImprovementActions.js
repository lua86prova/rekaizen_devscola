const ImprovementActions = require ('../services/improvementActions')
const Issues = require ('../services/issues')

class RetrieveImprovementActions {
  static do(issue) {
    const ids = Issues.service.retrieveImprovementActions(issue)
    const improvementActions = ImprovementActions.service.retrieve(ids)

    return {
      improvementActions: improvementActions
    }
  }
}

module.exports = RetrieveImprovementActions
