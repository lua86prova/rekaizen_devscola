const Issues = require('../services/issues')
const ImprovementActions = require('../services/improvementActions')

class RemoveImprovementAction {
  static do(improvementAction) {
    ImprovementActions.service.remove(improvementAction.id)

    const issue = Issues.service.removeImprovementAction(improvementAction.issue, improvementAction.id)

    return issue
  }
}

module.exports = RemoveImprovementAction
