const Issues = require('../services/issues')

class RemoveAnalysis {
  static do(analysis) {

    const issue = Issues.service.removeAnalysis(analysis.issue, analysis.id)
    return issue
  }
}

module.exports = RemoveAnalysis
