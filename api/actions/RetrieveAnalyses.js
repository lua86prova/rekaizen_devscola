const Issues = require('../services/issues')

class RetrieveAnalyses {
  static do(issue) {
    const positives = Issues.service.retrievePositives(issue)
    const negatives = Issues.service.retrieveNegatives(issue)
    return {
      positives: positives,
      negatives: negatives
    }
  }
}

module.exports = RetrieveAnalyses
