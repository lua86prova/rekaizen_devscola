const CreateGoodPractice = require('./CreateGoodPractice')
const CreateLessonLearned = require('./CreateLessonLearned')
const CreateNegative = require('./CreateNegative')
const CreatePositive = require('./CreatePositive')
const CreateImprovementAction = require('./CreateImprovementAction')
const GenerateReport = require('./GenerateReport')
const RemoveAnalysis = require('./RemoveAnalysis')
const RemoveImprovementAction = require('./RemoveImprovementAction')
const RetrieveImprovementActions = require('./RetrieveImprovementActions')
const RetrieveGoodPractices = require('./RetrieveGoodPractices')
const RetrieveLessonsLearned = require('./RetrieveLessonsLearned')
const RetrieveTimeboxes = require('./RetrieveTimeboxes')
const RetrieveAnalyses = require('./RetrieveAnalyses')
const CreateIssue = require('./CreateIssue')
const TranslateAll = require('./TranslateAll')

const Actions = {
  translateAll: TranslateAll,
  retrieveTimeboxes: RetrieveTimeboxes,
  retrieveImprovementActions: RetrieveImprovementActions,
  retrieveLessonsLearned: RetrieveLessonsLearned,
  retrieveGoodPractices: RetrieveGoodPractices,
  retrieveAnalyses: RetrieveAnalyses,
  removeAnalysis: RemoveAnalysis,
  removeImprovementAction: RemoveImprovementAction,
  generateReport: GenerateReport,
  createPositive: CreatePositive,
  createNegative: CreateNegative,
  createLessonLearned: CreateLessonLearned,
  createGoodPractice: CreateGoodPractice,
  createImprovementAction: CreateImprovementAction,
  createIssue: CreateIssue
}

module.exports = Actions
