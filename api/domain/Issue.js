const SecureRandom = require('../libraries/SecureRandom')
const Dates = require('../libraries/Dates')

class Issue {
  constructor(description, timebox){
    this.NOT_FINALIZED = null

    this.id = this.generateId()
    this.birthday = this.generateTimestamp()
    this.description = description
    this.timebox = timebox
    this.conclusions = []
    this.analyses = []
    this.improvementActions = []
    this.finalizedAt = this.NOT_FINALIZED
  }

  serialize() {
    return {
      'id': this.id,
      'description': this.description,
      'timebox': this.timebox,
      'birthday': this.birthday,
      'conclusions': this.conclusions,
      'improvementActions': this.improvementActions,
      'analyses': this.analyses,
      'finalizedAt': this.finalizedAt
    }
  }

  generateId() {
    return SecureRandom.uuid()
  }

  generateTimestamp(){
    return Dates.generateTimestamp()
  }

  isEqualTo(issue) {
    return issue == this.id
  }

  addConclusion(birthday) {
    this.conclusions.push(birthday)
  }

  addImprovementAction(id) {
    this.improvementActions.push(id)

    return id
  }

  retrieveImprovementActions() {
    return this.improvementActions
  }

  removeImprovementAction(id) {
    this.improvementActions = this.improvementActions.filter(improvementActionId => improvementActionId != id)
  }

  addAnalysis(analysis) {
    this.analyses.push(analysis)
  }

  removeAnalysis(id) {
    this.analyses = this.analyses.filter(analysis => !analysis.is(id))
  }

  retrieveNegatives () {
    const negatives = this.analyses.filter(analysis => analysis.isNegative())
    return negatives
  }

  retrievePositives () {
    const positives = this.analyses.filter(analysis => analysis.isPositive())
    return positives
  }

  finalize() {
    this.finalizedAt = this.generateTimestamp()
  }
}

module.exports = Issue
