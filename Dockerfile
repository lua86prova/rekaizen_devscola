FROM ruby:2.5.3-alpine3.9

WORKDIR /opt/deploy
RUN apk add git curl
RUN gem install dpl

COPY . /opt/deploy

CMD [ "./scripts/deploy" ]
