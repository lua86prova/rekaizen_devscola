#Rekaizen

## Development

### Development dependencies

- `Docker version 18.06.1-ce` or higher.
- `docker-compose version 1.18.0` or higher.

### Running the application for the first time (building the docker image)

You only need to run the following command in the project folder's root:

`docker-compose up --build`

> Remember to shutdown the docker conatiner with `docker-compose down` when you are done with your work.

### Running for development

After running the application like explained before, you must run in a new terminal session:

`docker-compose exec app npm run build`

> Remember to run this command every time you want to check your changes or use `docker-compose exec app npm run build-watch` instead for automatic rebuilding every time you make any changes in JS files.
> IMPORTANT!!! Remember to stop the watch runtime before launching the end to end tests.

## Tests

### Running the application test

After running the application you must execute:

- `docker-compose exec app npm run test-all`

If you only want to run the unitary component tests, you must run:
s
- `docker-compose exec app npm run test-unit`

If you only want to run the end to end tests, execute:

- `docker-compose exec app npm run build-test` && `docker-compose exec e2e npm run test-all`

### Running the API test

- `docker-compose exec api npm run test-all`

### Running all the tests

After running the application, execute:

- `sh run-all-tests.sh`

### Running e2e test in local machine

You can open the cypress interface locally. Execute the following command in the e2e folder:

- `CYPRESS_api_server=http://0.0.0.0:3001 CYPRESS_baseUrl=http://0.0.0.0:3000 npx cypress open`

## Deploy

### Deploying to Demo

At the present, we have two machines in Heroku to deploy the application for demo purposes, you can check it out at:

APP: `https://rekaizen.herokuapp.com`
API: `https://rekaizen-api.herokuapp.com`

> If you are in Demo and you want to reset the application data, you must make a `POST` to `https://rekaizen-api.herokuapp.com/clean`

The CI automatically deploys the project when all the tests are green.
