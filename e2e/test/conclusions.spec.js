import Fixtures from './Fixtures'
import Issue from './pages/Issue'

describe('Conclusions', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  after(() => {
    Fixtures.cleanCollections()
  })

  it('creates a Good Practice', () => {
    const description = "good practice text"

    const issue = new Issue()
      .createIssue()
      .openNewGoodPractice()
      .closeConclusionModal()
      .openNewGoodPractice()
      .fillConclusion(description)
      .addConclusion()

    expect(issue.includes('¡Habéis declarado una buena práctica!')).to.be.true
  })

  it('creates a Lesson Learned', () => {
    const description = "lesson learned text"

    const issue = new Issue()
      .createIssue()
      .openNewLessonLearned()
      .closeConclusionModal()
      .openNewLessonLearned()
      .fillConclusion(description)
      .addConclusion()

    expect(issue.includes('¡Habéis declarado un nuevo aprendizaje!')).to.be.true
  })
})
