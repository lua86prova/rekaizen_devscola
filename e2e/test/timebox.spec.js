import Issue from './pages/Issue'

describe('Rekaizen', () => {

  it('show extend options when click on more options', () => {
    const otherOptionNumber = '60'

    const issue = new Issue()
      .startIssue()
      .addDescription()
      .clickOnMoreOptions()

    expect(issue.includes(otherOptionNumber)).to.be.true
  })
  it('the logo decreases when a timebox is selected', () => {
    const issue = new Issue()
      .createIssue()

    expect(issue.isVisible('.logo')).to.be.true
    expect(issue.hasCSSAtribute('.logo', 'animation-duration')).to.be.true
  })
})
